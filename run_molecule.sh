#!/bin/bash

set -e -u -o pipefail

podman build -t local/ansible -f Containerfile .

collansible="collections/ansible_collections/"

privpodrun() {
  # Molecule will need to be privileged for testing systemd
  podman run -it --rm \
    -e C_TAG="${C_TAG:-latest}" \
    --privileged \
    --systemd=always \
    "$@"
}

if [[ "$(pwd)" == *"${collansible}"* ]]; then
  colldir="$(pwd | sed "s|^.*${collansible}||")"
  # echo "colldir: ${colldir}"
  reldir="$(echo "${colldir}" | perl -pe 's/[^\/]+?(\/|$)/..\//g')../${collansible}"
  # echo "reldir: ${reldir}"
  privpodrun -v "${reldir}":"/${collansible}" -w "/${collansible}${colldir}" local/ansible "$@"
else
  privpodrun -v "$(pwd)":/workdir/ local/ansible "$@"
fi
