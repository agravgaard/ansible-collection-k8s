# [Ansible Collection - agravgaard.k8s](https://galaxy.ansible.com/ui/namespaces/agravgaard/)

This collection is meant for setting up kubernetes on Turing Pi[^1] and raspberry pi nodes.

> :warning: If you got here from [grepler's post on the Talos repo](https://github.com/siderolabs/talos/issues/3990),
> be aware the repo no longer supports Talos, [since this tag](https://gitlab.com/agravgaard/ansible-collection-k8s/-/tree/Talos?ref_type=tags)

It will expect Manjaro, Ubuntu or Raspbian[^2] to be installed on each node in the cluster.

It assumes the localhost, i.e. the machine running the playbook, has `kubectl` configured.

The nodes will likely need a reboot, if it's not automatically triggered, after first configuration,
for some settings to take effect. I've included [a reboot playbook](./playbooks/reboot.yaml) for this purpose.

[^1]: The i2c role is technically specific to Turing Pi v1.
[^2]: I only have Manjaro (CM3+) and Ubuntu (RK1) installed on my cluster, so Raspbian will only be supported through molecule tests.
Please submit issues/merge requests for any issues you encounter.

## Prerequisites

### Playbooks

There are two options for [Running the playbook](#running-the-playbook), either [Containerized](#containerized) or [Native](#native):

Host (Containerized):
 - `~/.kube/config`
 - ssh access to nodes
 - `podman`

Host (Native):
 - `kubectl` and config
 - `kubeadm` for creating the join token
 - `ssh` access to nodes
 - `ansible`

Nodes:
 - OS installed
 - Accesible over ssh
 - Remember to edit the inventory
 - Initial system update (there will typically be some manual choices on package options)

## Roles

### prereq

Installs prerequisites for running the kubelet

### i2c

You should only use the i2c role with nodes in a Turing Pi 1 board.

This role depends on the `/boot/config.txt` file to be present and creates scripts,
which are only useful with the interface provided by Turing Pi 1.

## Running the playbook

You can either run the playbook as usual with `ansible-playbook`, or using in a podman container with the script
`./run_ansible.sh`

### Containerized

This approach requires podman, see the script for details.
```bash
./run_ansible.sh agravgaard.k8s.my_playbook -i inventory --ask-become-pass
```

It creates a volume called `collections` in which it will install the collection and its galaxy dependencies.
You can delete this volume when you wish: `podman volume rm collections`

### Native

If you prefer having ansible installed locally, you should be able to run it as you usually would.

Install ansible, and the collections from galaxy, then:
```bash
ansible-playbook agravgaard.k8s.my_playbook -i inventory --ask-become-pass
```

## Running the molecule tests

If you want to run the tests locally, I'd suggest using the `./run_molecule.sh` scripts.
It essentially mimics the gitlab-ci pipeline. And runs molecule containerized in podman.

To keep the molecule session for debugging the script can be run as `./run_molecule.sh bash` to get a shell. Then
run the desired molecule commands there.

Otherwise,`./run_molecule.sh molecule test` should run the full e2e test.
