#!/bin/bash

set -e -u -o pipefail

podman build -t local/ansible -f Containerfile .

podman volume create --ignore collections

ansible_home="/root"

podrun() {
  podman run -it --rm \
    -v collections:"${ansible_home}/.ansible/collections/" \
    -v "$(pwd)":/workdir/ \
    -v "$HOME/.kube/":"${ansible_home}/.kube/":ro \
    -v "$HOME/.ssh/":"${ansible_home}/.ssh/":ro \
    -v "$HOME/vault.key":"${ansible_home}/vault.key":ro \
    -e ANSIBLE_VAULT_PASSWORD_FILE="${ansible_home}/vault.key" \
    local/ansible "$@"
}

version="$(grep '^version:' galaxy.yml | sed -E 's/^version:\s+([0-9\.]+)$/\1/')"
local_build="./agravgaard-k8s-${version}.tar.gz"

podrun ansible-galaxy collection install -r ./requirements.yml
podrun ansible-galaxy collection build --force
podrun ansible-galaxy collection install "${local_build}" --force
podrun ansible-playbook "$@"
