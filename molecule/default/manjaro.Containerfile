# Mimic Manjaro ARM Minimal 23.02
FROM docker.io/manjarolinux/base:20230226

RUN cat <<-EOF >/etc/pam.d/su
#%PAM-1.0
auth            sufficient      pam_rootok.so
auth            sufficient      pam_wheel.so trust use_uid
auth            required        pam_wheel.so use_uid
auth            required        pam_unix.so
account         required        pam_unix.so
session         required        pam_unix.so
password        include         system-auth
EOF

RUN cat <<-EOF >/boot/config.txt
#
# See /boot/overlays/README for all available options
#

#gpu_mem=64
initramfs initramfs-linux.img followkernel
kernel=kernel8.img
arm_64bit=1
disable_overscan=1
dtparam=krnbt=on

#enable sound
dtparam=audio=on
#hdmi_drive=2

#enable vc4
dtoverlay=vc4-kms-v3d
max_framebuffers=2
disable_splash=1
EOF

RUN cat <<-EOF >/boot/cmdline.txt
root=PARTUUID=d89b0549-02 rw rootwait console=serial0,115200 console=tty3 selinux=0 quiet splash plymouth.ignore-serial-consoles smsc95xx.turbo_mode=N dwc_otg.lpm_enable=0 usbhid.mousepoll=8 audit=0
EOF

# Pre-update to speed up tests
RUN pacman -Syu --noconfirm

# The Manjaro image oddly returns 1 on `pacman -Qe`
# until a package has been marked explicitly installed.
# This behaviour is different from the native system image.
# `pacman -Qe` is part of the community.pacman operations.
# So we set preinstalled manjaro packages with no dependencies as explicit
RUN pacman -Qtq | xargs pacman -D --asexplicit

RUN useradd -m -G wheel bun

CMD [ "/sbin/init" ]
